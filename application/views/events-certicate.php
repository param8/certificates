
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->
		<div class="content-header">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="page-title"><?=$page_title=='Agent' ? '<i class="fa fa-user-secret"></i>' : '<i class="fa fa-user"></i>'?> <?=$page_title?></h3>
					<div class="d-inline-block align-items-center">
						<nav>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
								<li class="breadcrumb-item" aria-current="page"><?=$page_title?></li>
								<li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>

		<!-- Main content -->
		<section class="content">
		  <div class="row">
		  <div class="col-md-4 col-lg-4">
			   <div class="box"> 
            <div class="box-header with-border">
                <h3 class="box-title">All <?=$page_title?></h3>
            </div>
          </div>
        </div>
        <div class="col-md-8 col-lg-8 ">
          <div class="box "> 
            <div class="box-header with-border">
              <!-- <a href="<?//=base_url('create-user/'.base64_encode($page_title))?>" class="btn btn-primary btn-sm float-right" >Add <?//=$page_title?> <i class="fa fa-plus"></i></a>
            <span class="ml-2">
            <a href="javascript:void(0)" class="btn btn-success btn-sm " data-toggle="modal" data-target="#bulkUploadModal" data-whatever="@mdo">Bulk Certificates <i class="fa fa-upload"></i></a>
            </span> -->
           

            <!-- <span class="ml-2">
            <a href="javascript:void(0)" class="btn btn-success btn-sm " data-toggle="modal" data-target="#admitCardUploadModal" data-whatever="@mdo">Upload Admit Card  <i class="fa fa-upload"></i></a>
            </span> -->
            </div>
          </div>
				</div>
			  <div class="box">
				<div class="box-header with-border">

				<!-- /.box-header -->
				<div class="box-body">
        <div class="row">
          <div class="col-md-6 col-lg-6 ">
          <lable>Download Certificate By Email</lable>
          <select class="form-control js-example-basic-single" name="user_email" id="user_email" onchange="setSessionUserEmail(this.value)">
            <option value="">Select Email</option>
            <?php foreach($events as $event){?>
              <option value="<?=$event->email?>"><?=$event->name.'('.$event->email .')'?></option>
              <?php } ?>
          </select>

        </div>

      </div>
      <hr>
   
					<div class="table-responsive">
					  <table id="" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
						<thead>
							<tr>
                <th>SNO</th>
								<th>Action</th>
								<th>Organizer</th>
								<th>Name</th>
								<th>Email</th>
								<th>Mobile</th>
								<th>Created Date</th>
						
							</tr>
						</thead>
						<tbody id="show_data">
          
				
						</tbody>				  
					
					</table>
					</div>              
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->          
			</div>
			<!-- /.col -->
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  
	  </div>
  </div>
  <!-- /.content-wrapper -->

  <div class="modal fade" id="bulkUploadModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Upload <?=$page_title?> Certificates</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?=base_url('user/bulk_import_certificate')?>" id="bulkUploadCeritificate" method="POST" enctype="multipart/form-data">
      <div class="modal-body">
	  <div class="form-group">
            <label for="name" class="col-form-label">Event Organizer:</label>
			<select class="form-control" name="eventID" id="eventID">
               <option value="">Select Event Organizer</option>
			   <?php foreach($events as $event){?>
				<option value="<?=$event->id?>"><?=$event->name?></option>
				<?php } ?>
			</select>
           </div>
          <div class="form-group">
            <label for="name" class="col-form-label">Cerificate:</label>
            <input type="file" class="form-control" name="certificate" id="certificate">
           </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
		<button type="button" onclick="downloadSample()" class="btn btn-success" >Download Sample <i class="fa fa-download"></i></button>
        <button type="submit" name="submit" class="btn btn-primary">Upload <i class="fa fa-upload"></i></button>
      </div>
      </form>
    </div>
  </div>
</div>






<script>

$("form#bulkUploadCeritificate").submit(function(e) {
     $(':input[type="submit"]').prop('disabled', true);
     e.preventDefault();    
     var formData = new FormData(this);
     $.ajax({
       url: $(this).attr('action'),
       type: 'POST',
       data: formData,
       cache: false,
       contentType: false,
       processData: false,
       dataType: 'json',
       success: function (data) {
         if(data.status==200) {
           toastr.success(data.message);
  				setTimeout(function(){
                location.reload();
           }, 1000) 
  
         }else if(data.status==403) {
           toastr.error(data.message);
           $(':input[type="submit"]').prop('disabled', false);
         }else{
           toastr.error('Unable to upload documents');
           $(':input[type="submit"]').prop('disabled', false);
         }
       },
       error: function(){} 
     });
   });




  function downloadSample(){
    location.href="<?=base_url('user/download_sample')?>";  
  }

  function setSessionUserEmail(email){
    $.ajax({
      url: '<?=base_url('user/get_user_certificate')?>',
      type: 'POST',
      data: {email},
      success: function (data) {
        $('#show_data').html(data);
        
      }
      });
  }


</script>