<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>I Card</title>
</head>
<body style="background: rgb(204, 204, 204);font-family: sans-serif;">
    <div class="A4" style="background: white;
    display: block;
    margin: 0 auto;position: relative;
    margin-bottom: 0.5cm;
    border:2px solid #00aceb; width: 20cm;">

  <div class="admitcard" style="background-color: #f4f4f4;  padding: 20px;">
    <table style="width: 100%;">
    <tr>
      <td colspan="2" align="center"><span><b>Registered Company: U80902UP20200PC125616</b></span>
 
    </td>
    </tr>
    <tr>
      <td style="width: 12%; text-align: center;"><img src="./logo.jpg" alt="" style="width: 64%;    border: 2px solid #00aceb;
    border-radius: 100%;"></td>
      <td><p style="    font-size: 26px;font-weight: bold;text-align: center;font-family: sans-serif;  color: #da2734;    margin: 0;">Brain Server Computer & Vocational Institute</p>
        <small  style="    font-size: 15.2px;   text-align: center;  display: inline-block;  font-weight: 600;">A Unit of Ramkumar Vidya Shiksha Sanshthan (RVSS) Society Act-21, 1860 AN ISO 9001-2015 CERTIFIED</small>
      </td>
    </tr>
    <tr>
      <td colspan="2"><br><br></td>
    </tr>
    <tr>
    <td colspan="2">
      <table style="width: 100%;">
      <tr>
        <td>
        <div class="user_info" style="    padding-left: 4%;">
          <div class="user_form" style="    display: flex;    margin-bottom: 10px;align-items: center;">
            <label for="" style=" z-index:9;    font-weight: bold; width: 25%;">Name </label> <b style="padding: 0 8px;">:</b> <input type="text" value="<?=$user->student_name?>" disabled style="        outline: none;  font-size: 16px;  background-color: #4577884f;width: 61%;border: none;border-bottom: 2px solid #00aceb; padding: 0 10px;;height: 32px;z-index: 9;">
          </div>
          <div class="user_form" style="    display: flex;    margin-bottom: 10px;align-items: center;">
            <label for="" style=" z-index:9;    font-weight: bold; width: 25%;">Mobile No </label> <b style="padding: 0 8px;">:</b> <input type="text"  value="<?=$user->student_contact?> " disabled style="       outline: none;  font-size: 16px;   background-color: #4577884f;width: 61%;border: none;border-bottom: 2px solid #00aceb; padding: 0 10px;z-index: 9;;height: 32px;">
          </div>
          <div class="user_form" style="    display: flex;    margin-bottom: 10px;align-items: center;">
            <label for="" style=" z-index:9;    font-weight: bold; width: 25%;">Join Date </label> <b style="padding: 0 8px;">:</b> <input type="text"  value="<?=date('d-m-Y',strtotime($user->created_at))?>" disabled style="         outline: none; font-size: 16px;  background-color: #4577884f;width: 61%;border: none;border-bottom: 2px solid #00aceb; padding: 0 10px;;height: 32px;z-index: 9;">
          </div>
        
           
        </div>
      </td>
      <td style="width:20% ; vertical-align: initial;   padding-top: 6px;"><img style="width: 100%;    height: 113px;" src="<?=base_url($user->profile_pic)?>" alt=""></td>
   
      </tr>
      <tr>
        <td colspan="2" style="    padding-left: 3%;">
           <div class="user_form" style="    display: flex;    margin-bottom: 10px;align-items: center;">
            <label for="" style=" z-index:9;    font-weight: bold; width: 20%;">Course Name  </label> <b style="padding: 0 8px;">:</b> <span style="    font-weight: 500;
    font-size: 21px;"><?=$user->course?></span>
          </div>
        </td>
      </tr>
      <tr>
        <td colspan="2"><span style="    color: red; text-align: center; display: block; font-size: 22px; font-weight: 800;">Kshitij Institute Of Paramedical And Higher Studies</span></td>
      </tr>
      <tr>
        <td colspan="2" align="center">
          <span style="font-weight: 800;
          font-size: 19px;">Institute Add.</span>
          <p style="    font-size: 20px;">Near St Pauls School,Near Sabji Mandi Road Marathipura, Maudaha</p>
          <p>Institute Mobile<br> <b>8090664282</b></p>
        </td>
      </tr>
      </table>
    </td>  
    </tr>
   
 
    </table>
  </div>
</div>
<center>
<button onclick="window.print()" style="background-color: #4CAF50; /* Green */
            border: none;
            color: white;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
            -webkit-transition-duration: 0.4s; /* Safari */
            transition-duration: 0.4s;">Print I Card</button>
            </center>
</body>
</html>