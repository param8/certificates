<style>
  .cf1{     
  position: initial!important;
  opacity: 1!important;
  }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <div class="container-full">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <h3 class="page-title"><i class="fa fa-user"> <?=$page_title?></i></h3>
          <div class="d-inline-block align-items-center">
            <nav>
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                <li class="breadcrumb-item" aria-current="page"><?=$page_title?></li>
                <li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12 col-lg-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><?=$page_title?></h3>
            </div>
          </div>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="">
        <form action="<?=base_url('user/update')?>" id="editusers" method="POST" enctype="multipart/form-data">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="name" class="col-form-label"> Name:</label>
                <input type="text" class="form-control" name="name" id="name" value="<?=$user->name?>">
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="name" class="col-form-label"> Event Title:</label>
                <input type="text" class="form-control" name="title" id="title" value="<?=$user->title?>">
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="email" class="col-form-label">Email:</label>
                <input type="text" class="form-control" name="email" id="email" value="<?=$user->email?>">
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="email" class="col-form-label">Contact No:</label>
                <input type="text" class="form-control" name="contact" id="contact" value="<?=$user->contact?>" maxlength="10" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="email" class="col-form-label">Address:</label>
                <textarea  class="form-control" name="address" id="address"> <?=$user->address?></textarea>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="email" class="col-form-label">Contant:</label>
                <textarea  class="form-control" name="event_contant" id="event_contant"> <?=$user->event_contant?></textarea>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="email" class="col-form-label">Organize Date:</label>
                <input type="text"  class="form-control" name="orgnize_date" id="orgnize_date" value="<?=$user->orgnize_date?>">
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
              <label for="profile_pic" class="col-form-label">Logo:</label>
              <input type="file" name="profile_pic" id="profile_pic" class="form-control" placeholder="Enter Name Here"><span><img src="<?=base_url($user->profile_pic)?>" width="150" height="100"></span>
            </div>
            <?php $certificates = array('1','2','3','4','5','6','7','8')?>
            <div class="col-lg-12 col-md-12 col-sm-12">
              <label for="" class="col-form-label">Cerificates:</label>
              <div class="row">
                <?php foreach($certificates as $certificate){?>
                <div class="col-lg-2 col-md-2 col-sm-12">
                  <span><input type="radio" name="cerificate" id="cerificate<?=$certificate?>" class="cf1" value="<?=$certificate?>" <?=$user->cerificate==$certificate ? 'checked' : ''?>><span><a href="<?=base_url('public/certifcate_images/certificate'.$certificate.'.png')?>" target="_blank" title="View Certificate"><img src="<?=base_url('public/certifcate_images/certificate'.$certificate.'.png')?>" width="100" height="100"></a></span><span>
                </div>
                <?php } ?>
              </div>
            </div>
            <hr>
            <!-- <div class="col-lg-12 col-md-12 col-sm-12">
              <div class="col-lg-4 col-md-4 col-sm-12">
                <label for="email" class="col-form-label">Name:</label>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12">
                <label for="email" class="col-form-label">Signature:</label>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12">
                <label for="email" class="col-form-label">
                  Action:
                  <div class="input-group-btn">   
                    <button class="btn btn-success add-more" type="button"><i class="glyphicon glyphicon-plus"></i> </button>  
                  </div>
                </label>
              </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12">
              <div class="form-group">
                <div class="input-group control-group after-add-more">
                  <div class="col-lg-4 col-md-4 col-sm-12">
                 
                    <input type="text" name="signature_name[]"  class="form-control" placeholder="Enter Name Here">
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-12">
           
                    <input type="file" name="signature[]"  class="form-control" placeholder="Enter Name Here">
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-12">
         
                    <div class="input-group-btn">   
                      <button class="btn btn-success add-more" type="button"><i class="glyphicon glyphicon-plus"></i> </button>  
                    </div>
                  </div>
                </div>
                <div class="">
                <?php 
                  // $signatures =  json_decode($user->signature);
                  // //print_r($signatures);
                  // foreach($signatures as $signature){
                  //   foreach($signature as $key=>$sign){
                    //print_r($signature);
                  ?>
                  <div class="control-group input-group" style="margin-top:10px">
                    <div class="col-lg-4 col-md-4 col-sm-12">
                      <input type="text" name="signature_name[]" value="<?//=$key?>"  class="form-control" placeholder="Enter Name Here">
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                      <input type="file" name="signature[]" value="<?//=$sign?>" class="form-control" placeholder="Enter Name Here">
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                      <div class="input-group-btn">   
                        <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i> </button>  
                      </div>
                    </div>
                    <?php //} }?>
                  </div>
                </div>
                <div class="copy hide">
                  <div class="control-group input-group" style="margin-top:10px">
                    <div class="col-lg-4 col-md-4 col-sm-12">
                      <input type="text" name="signature_name[]"  class="form-control" placeholder="Enter Name Here">
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                      <input type="file" name="signature[]"  class="form-control" placeholder="Enter Name Here">
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                      <div class="input-group-btn">   
                        <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i> </button>  
                      </div>
                    </div>
                  </div>
                </div> -->
              <div class="modal-footer">
                <button type="submit" name="submit" class="btn btn-primary">Submit</button>
              </div>
        </form>
        </div>
        <!-- /.box-body -->
        </div>
        <!-- /.box -->          
      </div>
      <!-- /.col -->
  </div>
  <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
</div>
<!-- /.content-wrapper -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script type="text/javascript">
  $("form#editusers").submit(function(e) {
     $(':input[type="submit"]').prop('disabled', true);
     e.preventDefault();    
     var formData = new FormData(this);
     formData.append("user_type", '<?= $user_type?>');
     formData.append("user_id", '<?= $user_id?>');
     $.ajax({
       url: $(this).attr('action'),
       type: 'POST',
       data: formData,
       cache: false,
       contentType: false,
       processData: false,
       dataType: 'json',
       success: function (data) {
         if(data.status==200) {
           toastr.success(data.message);
          
  				setTimeout(function(){
                      window.location="<?=base_url('events')?>";
           }, 1000) 
  
         }else if(data.status==403) {
           toastr.error(data.message);
           $(':input[type="submit"]').prop('disabled', false);
         }else{
           toastr.error('Unable to add site info');
           $(':input[type="submit"]').prop('disabled', false);
         }
       },
       error: function(){} 
     });
   });
  
   function endDateValidate(start_date){
      $('#endDate_div').html(' <div class="form-group"><label for="name" class="col-form-label">End Date:</label><input type="date" class="form-control" min="'+start_date+'" name="end_date" id="end_date" placeholder="End Date"></div>');
    }
  
    $(document).ready(function() {  
  
  $(".add-more").click(function(){  
      var html = $(".copy").html();  
      $(".after-add-more").after(html);  
  });  
  
  $("body").on("click",".remove",function(){   
      $(this).parents(".control-group").remove();  
  });  
  
  }); 
</script>