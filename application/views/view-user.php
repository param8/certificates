<style>
  .cf1{     
    position: initial!important;
    opacity: 1!important;
  }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <div class="container-full">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <h3 class="page-title"><i class="fa fa-user"> <?=$page_title?></i></h3>
          <div class="d-inline-block align-items-center">
            <nav>
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                <li class="breadcrumb-item" aria-current="page"><?=$page_title?></li>
                <li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12 col-lg-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><?=$page_title?></h3>
            </div>
          </div>
        </div>
      </div>
      <!-- /.box-header -->
   
      <div class="">
        <form action="<?=base_url('user/store')?>" id="addusers" method="POST" enctype="multipart/form-data">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="name" class="col-form-label"> Name:</label>
                <input type="text" class="form-control" name="name" id="name" value="<?=$user->name?>" disabled>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="name" class="col-form-label"> Event Title:</label>
                <input type="text" class="form-control" name="title" id="title" value="<?=$user->title?>" disabled>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="email" class="col-form-label">Email:</label>
                <input type="text" class="form-control" name="email" id="email" value="<?=$user->email?>" disabled>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="email" class="col-form-label">Contact No:</label>
                <input type="text" class="form-control" name="contact" id="contact" value="<?=$user->contact?>" disabled maxlength="10" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="email" class="col-form-label">Address:</label>
                <textarea  class="form-control" name="address" id="address" disabled><?=$user->address?> </textarea>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="email" class="col-form-label">Contant:</label>
                <textarea  class="form-control" name="event_contant" id="event_contant" disabled><?=$user->event_contant?></textarea>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="email" class="col-form-label">Organize Date:</label>
                <input type="text"  class="form-control" name="orgnize_date" id="orgnize_date" value="<?=$user->orgnize_date?>" disabled>
              </div>
            </div>
          
            <div class="col-lg-4 col-md-4 col-sm-12">
              <label for="profile_pic" class="col-form-label">Logo:</label>
              <img src="<?=base_url($user->profile_pic)?>" width="100" height="100">
            </div>

            
            <div class="col-lg-12 col-md-12 col-sm-12">
                  <label for="" class="col-form-label">Cerificates:</label>
                <div class="row">
                  <div class="col-lg-2 col-md-2 col-sm-12">
                    <span><input type="radio" name="cerificate" id="cerificate1" class="cf1" value="1" <?=$user->cerificate==1 ? 'checked' : ''?>><span><a href="<?=base_url('public/certifcate_images/certificate1.png')?>" target="_blank" title="View Certificate"><img src="<?=base_url('public/certifcate_images/certificate1.png')?>" width="100" height="100"></a></span><span>
                  </div>

                  <!-- <div class="col-lg-2 col-md-2 col-sm-12">
                    <span><input type="radio" name="cerificate" id="cerificate2" class="cf1" value="2" <?=$user->cerificate==2 ? 'checked' : ''?>><span><a href="<?=base_url('public/certifcate_images/certificate2.png')?>" target="_blank" title="View Certificate"><img src="<?=base_url('public/certifcate_images/certificate2.png')?>" width="100" height="100"></a></span><span>
                  </div>

                  <div class="col-lg-2 col-md-2 col-sm-12">
                    <span><input type="radio" name="cerificate" id="cerificate3" class="cf1" value="3" <?=$user->cerificate==3 ? 'checked' : ''?>><span><a href="<?=base_url('public/certifcate_images/certificate3.png')?>" target="_blank" title="View Certificate"><img src="<?=base_url('public/certifcate_images/certificate3.png')?>" width="100" height="100"></a></span><span>
                  </div>

                  <div class="col-lg-2 col-md-2 col-sm-12">
                    <span><input type="radio" name="cerificate" id="cerificate4" class="cf1" value="4" <?=$user->cerificate==4 ? 'checked' : ''?>><span><a href="<?=base_url('public/certifcate_images/certificate4.png')?>" target="_blank" title="View Certificate"><img src="<?=base_url('public/certifcate_images/certificate4.png')?>" width="100" height="100"></a></span><span>
                  </div>

                  <div class="col-lg-2 col-md-2 col-sm-12">
                    <span><input type="radio" name="cerificate" id="cerificate5" class="cf1" value="5" <?=$user->cerificate==5 ? 'checked' : ''?>><span><a href="<?=base_url('public/certifcate_images/certificate5.png')?>" target="_blank" title="View Certificate"><img src="<?=base_url('public/certifcate_images/certificate5.png')?>" width="100" height="100"></a></span><span>
                  </div>

                  <div class="col-lg-2 col-md-2 col-sm-12">
                    <span><input type="radio" name="cerificate" id="cerificate6" class="cf1" value="6" <?=$user->cerificate==6 ? 'checked' : ''?>><span><a href="<?=base_url('public/certifcate_images/certificate6.png')?>" target="_blank" title="View Certificate"><img src="<?=base_url('public/certifcate_images/certificate6.png')?>" width="100" height="100"></a></span><span>
                  </div>

                  <div class="col-lg-2 col-md-2 col-sm-12">
                    <span><input type="radio" name="cerificate" id="cerificate7" class="cf1" value="7" <?=$user->cerificate==7 ? 'checked' : ''?>><span><a href="<?=base_url('public/certifcate_images/certificate7.png')?>" target="_blank" title="View Certificate"><img src="<?=base_url('public/certifcate_images/certificate7.png')?>" width="100" height="100"></a></span><span>
                  </div>

                  <div class="col-lg-2 col-md-2 col-sm-12">
                    <span><input type="radio" name="cerificate" id="cerificate8" class="cf1" value="8" <?=$user->cerificate==8 ? 'checked' : ''?>><span><a href="<?=base_url('public/certifcate_images/certificate8.png')?>" target="_blank" title="View Certificate"><img src="<?=base_url('public/certifcate_images/certificate8.png')?>" width="100" height="100"></a></span><span>
                  </div> -->
                </div>
              </div>
        
            <div class="col-lg-12 col-md-12 col-sm-12">
              <div class="form-group">
                <div class="input-group control-group after-add-more">
                  <div class="col-lg-4 col-md-4 col-sm-12">
                    <label for="email" class="col-form-label">Name:</label>
                   
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-12">
                    <label for="email" class="col-form-label">Designation:</label>
                   
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-12">
                    <label for="email" class="col-form-label">Signature:</label>
                    
                  </div>
                 
                </div>
                <div class="">
                  <div class="control-group input-group" style="margin-top:10px">
                  <?php 
                  $signatures =  json_decode($user->signature);
                  //print_r($signatures);
                  foreach($signatures as $key=>$signature){
                    
                    //print_r($signature);
                  ?>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                      <input type="text" name="signature_name[]" value="<?=$signature[1]?>" disabled class="form-control" placeholder="Enter Name Here">
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                      <input type="text" name="signature_name[]" value="<?=$signature[2]?>" disabled class="form-control" placeholder="Enter Name Here">
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                    <img src="<?=base_url($signature[0])?>" width="100" height="100">
                    </div>
                    <?php   } ?>
                  </div>
                </div>


              </div>
              
        </form>
        </div>
        <!-- /.box-body -->
        </div>
        <!-- /.box -->          
      </div>
      <!-- /.col -->
  </div>
  <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
  $("form#addusers").submit(function(e) {
     $(':input[type="submit"]').prop('disabled', true);
     e.preventDefault();    
     var formData = new FormData(this);
     formData.append("user_type", '<?= $user_type?>');
     $.ajax({
       url: $(this).attr('action'),
       type: 'POST',
       data: formData,
       cache: false,
       contentType: false,
       processData: false,
       dataType: 'json',
       success: function (data) {
         if(data.status==200) {
           toastr.success(data.message);
          
  				setTimeout(function(){
                      window.location="<?=base_url('events')?>";
           }, 1000) 
  
         }else if(data.status==403) {
           toastr.error(data.message);
           $(':input[type="submit"]').prop('disabled', false);
         }else{
           toastr.error('Unable to add site info');
           $(':input[type="submit"]').prop('disabled', false);
         }
       },
       error: function(){} 
     });
   });
  
   
</script>