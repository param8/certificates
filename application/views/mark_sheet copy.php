<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?=$page_title?></title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>

<body style="color:#fdfdfd; font-family:Segoe, 'Segoe UI', 'DejaVu Sans', 'Trebuchet MS', Verdana, sans-serif">
<div class="container">
  <?php if($user){?>
     <div style="width:70%;background:white;padding:25px;margin:15px auto;box-shadow: 0px 0px 10px rgb(0 0 0 / 13%);
    border-radius: 20px;">
   <center>

           <table width="750px" border="0" cellspacing="0" cellpadding="0" style="background:#fdfdfd;">
              <tbody>
			   <tr>                  
                 <td colspan="5"><img src="<?=base_url('public/pdf_img/top-main.jpg')?>" width="750" height="70"></td>   
                </tr>
                <tr>
                  <td align="left"><img src="<?=base_url('public/pdf_img/top-left.jpg')?>" width="61" height="156"></td>
                 <td colspan="3" align="center" style="color:black;font-size:15px;">
                  <p style="display: flex;justify-content: space-around;"><span style="font-weight: bold;">Regd. No. APEC/DMLT-U201910011053.. </span> <span style="font-weight: bold;">Roll No. <?=$user->roll_no?> </span></p>
                  <!-- <img src="logo.svg" width="130" style="margin-bottom: 10px;"><br>This is to acknowledge that -->
                  <div style="    text-align: center;    ">
                    <h2 style="color:#d777ac; font-weight: 900;font-size: 34px;"><?=$user->institute_name?> </h2>
                    <p style="font-size: 14px; font-weight: 700;  color: #9b9b9b;">JE Soclety UnderXXl of 1860 Central Govt. of India &amp; Punjab Government Amendment act of 1957</p>
                    <span style="font-size: 23px;  font-weight: 500;  font-style: oblique; color: #8d8686;">REGISTRATION NO. <?=$user->registration_no?></span>
                   </div>
                </td>  
                 <td align="right"><img src="<?=base_url('public/pdf_img/top-right.jpg')?>" width="66" height="156"></td>   
                </tr>
                <tr>
                  <td align="left"><img src="<?=base_url('public/pdf_img/left-b.jpg')?>" width="61" height="160"></td>                  
                  <td align="center" colspan="3">
                    <div style="    text-align: center; padding-bottom: 9px;  border-bottom: 3px solid #976868;">
                       <h3 style="  color: #635959;  font-size: 20px;  font-weight: 700;">HEAD DFFICE : 1307 A68,KANCHAN COLONY,NEAR ICICI BANK, PAKHOWAL ROAD, LUDHIANA 141013. </h3>
                      <h4 style=" color: #635959;    font-size: 16px;  font-weight: 700;">TELE/FAX : +91-161-2562075, +91-981514I685, Email : <a href="#" style="    text-decoration: none;">drparminders@yahoo.com</a></h4>
                    </div>
                    <h4 style="color: #922c64;    font-weight: 900;   font-style: italic;  ">ISO CERTIFIED - 9001 : 2015. </h4>
                  </td>                  
                  <td align="right"><img src="<?=base_url('public/pdf_img/right-b.jpg')?>" width="66" height="160"></td>
                </tr>
                 
                <tr>
                  <td align="left"><img src="<?=base_url('public/pdf_img/l200.jpg')?>" width="61" height="220"></td>                  
                  <td align="center" colspan="3" >
                   <table style="width: 100%;">
                    <tr><td   align="center"><strong style="color:#7f0506; font-size:36px">Memorandom of Marks</strong><br></td></tr>
                    <tr>
                      <td>
                        
                        <div class="user_name" style="  display: flex; margin-bottom: 9px;">
                          <div class="uname" style="    width: 25%;  color: #5f5e5e;  font-weight: 700;">Student Name</div>:
                          <div class="uinput" style="    width: 75%;   "><input type="text"  value="<?=$user->student_name?>" disabled style="font-weight: 800;  border: none;color: #000;    border-bottom: 2px dotted;"></div>
                        </div>
                      </td>
                      
                    </tr>
                    
                    <tr>
                    <tr>
                      <td colspan=" ">
                        <div class="user_name" style="  display: flex; margin-bottom: 9px;">
                          <div class="uname" style="    width: 25%;  color: #5f5e5e;  font-weight: 700;">Father Name </div>:
                          <div class="uinput" style="    width: 70%;   "><input type="text"  value="<?=$user->father_name?>" disabled style="font-weight: 800;  border: none;color: #000;    border-bottom: 2px dotted;"></div>
                        </div>
                      </td>
                    </tr>     
                  

                    <tr>
                      <td colspan=" ">
                        <div class="user_name" style="  display: flex; margin-bottom: 9px;">
                          <div class="uname" style="    width: 25%;  color: #5f5e5e;  font-weight: 700;">Mother Name </div>:
                          <div class="uinput" style="    width: 70%;   "><input type="text"  value="<?=$user->mother_name?>" disabled style="font-weight: 800;  border: none;color: #000;    border-bottom: 2px dotted;"></div>
                        </div>
                      </td>
                    </tr>
                    <tr>
                    <td>
                        <div class="user_name" style="  display: flex; margin-bottom: 9px;">
                          <div class="uname" style="    width: 25%;  color: #5f5e5e;  font-weight: 700;">Date of Birth</div>:
                          <div class="uinput" style="    width: 50%;   "><input type="text" value="<?=date('d-m-Y',strtotime($user->dob))?>" disabled style="font-weight: 800;  border: none;color: #000;    border-bottom: 2px dotted;"></div>
                        </div>
                      </td>
                    </tr>
                      <td colspan="">
                        <div class="user_name" style="  display: flex; margin-bottom: 9px;">
                          <div class="uname" style="    width: 25%;  color: #5f5e5e;  font-weight: 700;">Course Name</div>:
                          <div class="uinput" style="    width: 70%;   "><input type="text"  value="<?=$user->course?>" style="font-weight: 600;width: 100%;           border: none;color: #000;    border-bottom: 2px dotted;"></div>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td colspan="">
                        <div class="user_name" style="  display: flex; margin-bottom: 9px;">
                          <div class="uname" style="    width: 25%;  color: #5f5e5e;  font-weight: 700;">Institute Name</div> :
                          <div class="uinput" style="    width: 70%;   "><input type="text"  value="<?=$user->institute_name?>" style="font-weight: 600;          border: none;color: #d80d0d;width: 100%;    border-bottom: 2px dotted;"></div>
                        </div>
                      </td>
                    </tr>
                   </table>
                  </td>                  
                  <td align="right"><img src="<?=base_url('public/pdf_img/r200.jpg')?>" width="66" height="220"></td>
                </tr> 
                
                <tr>
                  <td align="left"><img src="<?=base_url('public/pdf_img/left-b_t.jpg')?>" width="61" height="384"></td>
                  <td colspan="3">
                    <table style="width:100%;    color: #000;">
                    <tr >
                      <td style="border: 2px solid; padding: 2px;    width: 5%; text-align: center;font-weight: 700;">SNo</td>
                      <td style="border: 2px solid; padding: 2px;    width: 5%; text-align: center;font-weight: 700;">SUBJECT</td>
                      <!-- <td style="border: 2px solid; padding: 2px; text-align: center;font-weight: 700; ">Pepper Code</td> -->
                      <td style="border: 2px solid; padding: 2px; text-align: center;font-weight: 700; ">Max Marks</td>
                      <td style="border: 2px solid; padding: 2px; text-align: center;font-weight: 700;">Min Marks</td>
                      <td style="border: 2px solid; padding: 2px; text-align: center;font-weight: 700;">Obtained Marks</td>
                          <!-- <td style="border: 2px solid; padding: 2px; text-align: center;font-weight: 700;">Result</td> -->
                    </tr>

                    <?php 
                      $markSheet = json_decode($user->mark_sheet);
                      $marks =array();
                      $total_marks = 0;
                      $countSubject = count($markSheet);
                      $total_max = 0;
                      foreach ($markSheet as $key => $values){
                       // print_r($values);
                        foreach($values as $key1=>$value){
                          $marks[] = $key1;
                          $vals =  explode(',',$value);
                      ?>
                    <tr>
                      <td style="border: 2px solid;padding: 4px;font-size: 18px;font-weight: 500; border-top: none; border-bottom: none;"> <?=$key+1?></td>
                      <td style="border: 2px solid;padding: 4px; text-align: center;font-weight: 600;white-space: nowrap;"> <?=$key1?> </td>
                      <?php 
                        $totalValuev=0;
                        for($i = 0; $i < count($vals); $i++){
                         // echo '<span style="color:red">'.$vals[0].'</span>'; 
                        
                          //echo '<span style="color:red">'.$val.'</span>';
                         ?>
                      <td style="border: 2px solid;padding: 4px; text-align: center;font-weight: 600;"><?=$vals[$i]?></td>
                      <?php   } ?>

                      <!-- <td style="border: 2px solid;padding: 4px; text-align: center;font-weight: 600; "> <b>PASS</b></td> -->
                    </tr>
          
              <?php }
                $total_max += $vals[0];
              $total_marks += $vals[2]; } ?>

            
                    <tr>
                      <td style="border: 2px solid;padding: 5px 0; font-size: 18px;font-weight: 500; height: 28px;  "> </td>
                      <td style="border: 2px solid;padding: 5px 0;  text-align: center;font-weight: 600; ">  </td>
                      <td style="border: 2px solid;padding: 5px 0;  text-align: center;font-weight: 600;"> </td>
                      <td style="border: 2px solid;padding: 5px 0;  text-align: center;font-weight: 600; "> </td>
                      <td style="border: 2px solid;padding: 5px 0;  text-align: center;font-weight: 600; "> </td>
                      <!-- <td style="border: 2px solid;padding: 5px 0;  text-align: center;font-weight: 600; "> </td> -->

                      <!-- <td style="border: 2px solid;padding: 5px 0;  text-align: center;font-weight: 600; "> <b></b></td> -->
                    </tr>
                    

                    <tr >
                      <td   style="border: 2px solid; font-size: 16px; padding: 6px;   font-weight: 700;     "> </td>

                      <td style="border: 2px solid; text-align: center;font-weight: 600;  font-weight: 700;">Total</td>
                     
                      <td style="border: 2px solid; text-align: center;font-weight: 600;  font-weight: 700; "><?=$total_max;?></td>

                      <td style="border: 2px solid; text-align: center;font-weight: 600;   "> </td>
                      <td style="border: 2px solid; text-align: center;font-weight: 600;  font-weight: 700; "><?=$total_marks?></td>
                      <!-- <td style="border: 2px solid; text-align: center;font-weight: 600;   "> </td> -->
                    </tr>

                  </table>
                  </td>
                  <td align="right"><img src="<?=base_url('public/pdf_img/right-b-t.jpg')?>" width="66" height="384"></td>
                </tr>
                
              
               
                 <tr>
                  <td align="left"><img src="<?=base_url('public/pdf_img/left-b.jpg')?>" width="61" height="100"> </td>                   
                  <td align="center"  width="300" valign="bottom">
                    <div class="image" style="    align-items: end;     border-bottom: 2px solid #dec286;   display: flex;">
                       
                      <div class="text" style="    width: 100%;"><b style="color:#000">Date: <?=date('d-m-Y',strtotime($user->created_at))?> </b> </div>
                    </div>
                  </td>
                  <td align="center" width="150"></td>
                  <td align="center" width="250" valign="bottom">
                   <!-- <p style="text-align:center; margin-bottom:0"><img src="<?//=base_url('public/pdf_img/signature.jpg')?>" style="width:135px;"></p> -->
               
                   <p style="color:#2E2E2E;font-size: 14px;text-align: center;margin-bottom: 0;font-weight: 700;">
                    Controller of Examination 
                   </p>
                 </td>                             
                  <td align="right"><img src="<?=base_url('public/pdf_img/right-b.jpg')?>" width="66" height="100"> </td>
                </tr> 
                <tr>
                  <td align="left"><img src="<?=base_url('public/pdf_img/left-b.jpg')?>" width="61" height="100"> </td>                   
                 <td colspan="3" style="color:#000; text-align: center;font-weight: 600;">ALL VERIFICATIONS  CAN BE DONE EITHER PHYSICALLY OR THROUGH EMAIL ONLY. </td>                            
                  <td align="right"><img src="<?=base_url('public/pdf_img/right-b.jpg')?>" width="66" height="100"> </td>
                </tr> 
                             
                <tr>
                  <td colspan="5"><img src="<?=base_url('public/pdf_img/bottom.jpg')?>" width="750" height="83"> </td>
                </tr>
              </tbody>
           </table>
           <button onclick="window.print()" style="background-color: #4CAF50; /* Green */
              border: none;
              color: white;
              padding: 15px 32px;
              text-align: center;
              text-decoration: none;
              display: inline-block;
              font-size: 16px;
              margin: 4px 2px;
              cursor: pointer;
              -webkit-transition-duration: 0.4s; /* Safari */
              transition-duration: 0.4s;">Print Mark Sheet</button>
           </center>
 
    </div>
    <?php }else{?>
      <h1 style="text-align: center; color: red;margin-top:30%"> No Mark Sheet Found</h1>
      <?php } ?>
</div>


</body>
</html>
