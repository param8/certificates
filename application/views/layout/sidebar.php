<aside class="main-sidebar">
  <!-- sidebar-->
  <section class="sidebar position-relative">
    <div class="multinav">
      <div class="multinav-scroll" style="height: 100%; overflow:auto">
        <!-- sidebar menu-->
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header">Dashboard</li>
          <li class="">
            <a href="<?=base_url('dashboard')?>">
              <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
              <span>Dashboard</span>
          </li>
          <li class="header">Components </li>
    

   <?php if($this->session->userdata('user_type')=='Admin'){?>
          <li class="treeview">
              <a href="#">
                <i class="fa fa-users"><span class="path1"></span><span class="path2"></span></i>
                <span>Event</span>
                <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
                </span>
              </a>
            <ul class="treeview-menu">
              <li><a href="<?=base_url('events')?>"><i class="fa fa-user"><span class="path1"></span><span class="path2"></span></i>Event Organizer</a></li>
              <li><a href="<?=base_url('events-cerificate')?>"><i class="fa fa-file"><span class="path1"></span><span class="path2"></span></i>Event Cerificate</a></li>
            </ul>
          </li>
          <li class="treeview">
            <a href="#">
            <i class="icon-Settings"><span class="path1"></span><span class="path2"></span></i>
            <span>Settings</span>
            <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
              <!-- <li><a href="<?=base_url('about')?>"><i class="icon-Image"><span class="path1"></span><span class="path2"></span></i>About US</a></li>
              <li> <a href="<?=base_url('enquiry-list')?>"><i class="fa fa-info-circle"><span class="path1"></span><span class="path2"></span></i><span>Enquiry List</span> -->
              <li><a href="<?=base_url('site-info')?>"><i class="icon-Image"><span class="path1"></span><span class="path2"></span></i>Site Setting</a></li>  
            </ul>
          </li>  
          
            <?php } if($this->session->userdata('user_type')=='Event'){?>		
            <li class="treeview">
              <a href="#">
                <i class="fa fa-users"><span class="path1"></span><span class="path2"></span></i>
                <span>Users</span>
                <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
                </span>
              </a>
            <ul class="treeview-menu">
              <li><a href="<?=base_url('students')?>"><i class="fa fa-user"><span class="path1"></span><span class="path2"></span></i>Students</a></li>
              
            </ul>
          </li>
            <?php } ?>		

        </ul>
      </div>
    </div>
  </section>
  <div class="sidebar-footer">
    <a href="javascript:void(0)" class="link" data-toggle="tooltip" title="" data-original-title="Settings" aria-describedby="tooltip92529"><span class="icon-Settings-2"></span></a>
    <a href="mailbox.html" class="link" data-toggle="tooltip" title="" data-original-title="Email"><span class="icon-Mail"></span></a>
    <a href="<?=base_url('authantication/adminLogout')?>" class="link" data-toggle="tooltip" title="" data-original-title="Logout"><span class="icon-Lock-overturning"><span class="path1"></span><span class="path2"></span></span></a>
  </div>
</aside>