<html>

<head>

  </style>
 
</head>

<body>
  <div class="certificate-container" style="  background-image: url();
        background-size: 100% 100%; padding: 50px;
    width:872px;background-color: #eee; -webkit-print-color-adjust: exact; 
    margin: auto; position: relative;">
    <img src="<?=base_url('certificate_image/bg-new.png')?>" alt="" style=" position: absolute; top: 0;left: 0; height: 100%; width: 100%;
        background-size: cover;
        background-position: center;
        ">
    <div class="certificate" style="padding: 25px;
            
            position: relative;">

      <div class="water-mark-overlay"></div>
   

      <div class="certificate-header" style="display: flex; margin-top: 2%; width:100%;  gap: 18px; margin-bottom:20px  ">
      
          <img src="<?=base_url('certificate_image/logo-new.png')?>"  class="logo" alt=""  style="     margin-top: 5%;   float: left;width: 15%;border-radius: 5px;margin-left: 10px; display:inline-block"> 
          <span style="     float: left; font-size: 70px;  font-weight: bold;    color: rgb(139 11 54);display:block;width: 65%;  -webkit-background-clip: text;   text-align: center;   -webkit-text-fill-color: transparent;
                "> Certificate  <span style="font-style: italic;color:#000;font-size: 30px; text-align:center;display:block ">of</span>

          <small> Participation</small>
        </span>
        <img src="<?=base_url('certificate_image/logo1.png')?>" class="logo" alt="" style="width: 15%;    margin-top: 5%;text-align:start">




      </div>


      <div class="certificate-body">

        <p style="text-align:center; display:block;      margin-bottom: 5px;     color: #ad2055;   font-size: 25px; font-weight: 600;">This is to certify that </p>
        <h1 style="    font-weight: bold;
                    font-size: 48px;
                    text-align: center;
                   
                    /* color: #0C5280; */
                    font-family: 'Pinyon Script', cursive;
                    /* font-size: 72px; */
                    width: 79%;
    margin: 0% auto;
                    background: -webkit-linear-gradient(#e0ae38, #c96d1b);border-bottom: 2px dotted #d47817;
                    -webkit-background-clip: text;
                    -webkit-text-fill-color: transparent;">
          <?=$name?>

        </h1>

        <!-- <div class="" style="display: flex; gap: 40px;justify-content: center;">
                    <div class="name" style="    width: 70%;font-family: system-ui;text-align: center;    color: #020100;">
                        <p class="certificate-title"><strong>1No. MMC/Accre. Cert/SPA-0108/2013 CPD Code -MMC/MAC/2022/D-016397 Appl no. 20221026242</strong></p>
                    </div>

                </div> -->
        <div class="certificate-content">


          <div class="text-center" style="margin-top: 4%;  margin-bottom: 4%;">
            <p class="topic-description text-muted" style="      width: 80%;
                            margin: auto;
                            text-align: center;   color: #492828;   font-size: 18px;">
              Participated and Presented in Midterm MOSCON Young Turks 2024, held on 4th and 5th May 2024.
            </p>
          </div>
        </div>
        <div class="certificate-footer text-muted text-center ">
          <div class="sign" style=" width: 100%;    text-align: center;  ">
            <div class="drname" style=" width: 14%;display: inline-block; text-align: center;">
              <img src="<?=base_url('certificate_image/santosh.png')?>" alt="" style="    width: 85px;
height: 40px;">
              <p style="margin: 0;    color: #3c232c;    font-size: 12px;
font-weight: bold;">Dr. Santosh Agrawal</p>
              <small style="    font-weight: bold;     font-size: 12px;"> President </small>
            </div>
            <div class="drname" style=" width: 18%;display: inline-block;  text-align: center;">
              <img src="<?=base_url('certificate_image/Vardhaman.png')?>" alt="" style="    width: 85px;
height: 40px;">
              <p style="margin: 0;    color: #3c232c;    font-size: 12px;
font-weight: bold;">Dr. Vardhaman Kankariya </p>
              <small style="    font-weight: bold;     font-size: 12px;">Chairman Scientific Committee </small>
            </div>
            <div class="drname" style="
    width: 14%;display: inline-block;  text-align: center;">
              <img src="<?=base_url('certificate_image/Atul.png')?>" alt="" style="    width: 85px;
height: 40px;">
              <p style="margin: 0;    color: #3c232c;    font-size: 12px;
font-weight: bold;">Dr. Atul Kadhane </p>
              <small style="    font-weight: bold;     font-size: 12px;">Hon. Secretary </small>
            </div>

            <div class="drname" style=" width: 17%;display: inline-block;  text-align: center;">
              <img src="<?=base_url('certificate_image/paranjpe.png')?>" alt="" style="    width: 85px;
height: 40px;">
              <p style="margin: 0;    color: #3c232c;    font-size: 12px;
font-weight: bold;">Dr. Mandar Paranjpe</p>
              <small style="    font-weight: bold;     font-size: 12px;">Hon. Treasurer </small>
            </div>
            <div class="drname" style="
    width: 14%; display: inline-block; text-align: center;">
              <img src="<?=base_url('certificate_image/aditi.png')?>" alt="" style="    width: 85px;
height: 40px;">
              <p style="margin: 0;    color: #3c232c;    font-size: 12px;
font-weight: bold;">Dr. Aditi Watve </p>
              <small style="    font-weight: bold;     font-size: 12px;">Secretary Scientific Committee </small>
            </div>


          </div>
        </div>
      </div>
    </div>
  </div>
</body>

</html>