<html>
  <head>
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?=base_url('uploads/PinyonScript-Regular.ttf')?>">
  <link href='https://fonts.googleapis.com/css?family=Pinyon Script' rel='stylesheet'>
 
    
 
  </head>
  <body>
    <div class="certificate-container" style="  background-image: url();
      background-size: 100% 100%;
      padding: 0px;
      width:18cm; 
      -webkit-print-color-adjust: exact; 
      margin: auto; position: relative;">
      <img src="<?=base_url('public/Certificate/final/bg.png')?>" alt="" style="    position: absolute;
        top: 0px;
        left:-40px;
        height: 100%;
        width:20cm;">
      <div class="certificate" style="padding: 25px;
        height: 24.4cmpx;
        padding: 50px;
        position: relative;">
        <div class="water-mark-overlay"></div>
        <!-- <a href="#" style="position: absolute;
          right: 11px;
          top: 52px;
          
          text-decoration: none;
          background: #fee57a;
          padding: 8px 18px;
          border-radius: 100px;
          color: #fff;">Download Now</a> -->
          <div style="height:90px"></div>
        <div class="certificate-header"  style="  text-align: center;    margin-top: 50px;  ">
        <img src="<?=base_url('public/Certificate/final/left_logo.png')?>" class="logo" alt=""   style="width: 19%; display:inline-block;    margin-bottom:0px;"> 
          <!-- <img src="<?//=base_url($profile_pic)?>" class="logo" alt="" style="width: 19%; display:inline-block;    margin-bottom:0px;">  -->
          <span style="
                    font-weight: bold;
            font-size: 60px; display:inline-block;
            margin-bottom: 20px;
            width:60%;
            color: #000000;
            font-family: ui-sans-serif;
            "> Certificate 
          </span>
          <!-- <img src="logo_r.png" class="logo" alt="" style="width: 14%;">  -->
          <img src="<?=base_url('public/Certificate/final/right_logo.jpg')?>" class="logo" alt="" style="width: 18%;"> 
        <div style="text-align: center;
          font-size: 32px;
          font-family: 'Pinyon Script', cursive;
          font-weight: bold;
          color: #804315;    margin-top: -24px;">
          <img src="<?=base_url('public/Certificate/attedance.png')?>" alt="" style="    width: 53%;">
        </div>
        <div class="certificate-body">
          <div class="certificate-content">
            <div class="text-center">
              <p class="topic-description text-muted"  style=" style="   
                            text-align: center;
                            color: #033a16;       
                              font-size: 44px;    margin-top: 19px;
                                font-weight: bold;    margin-bottom: 5%;> 
              <span class="att" style="   
                text-align: center;  ">  
               <?//=$title?>
               <img src="<?=base_url('public/Certificate/heading.png')?>" alt="" style="width: 90%;"><br>  
               </span> 
                <span  style=" 
              
                text-align: center;
                 color: #033a16;       
                font-size: 36px;    margin-top: 30px;
                font-weight: bold;    margin-bottom: 7%;
                font-family: 'Pinyon Script';
                "><u><?=$user_name?></u> <span>
              </p><br>
              <p style="    font-weight: 700;
                font-size: 22px;
                text-align: center;
                font-family: system-ui;
                margin-top: 0%;">
                  ATTENDED MOS MIDTERM CONFERENCE HELD
VIRTUALLY ON 15th & 16TH APRIL AS  DELEGATES.

                   <?//=$event_contant?>
                   </p>
                
  <p style="    font-weight: 700;
                font-size: 20px;
                text-align: center;
                font-family: system-ui;
                margin-top: 0%;"> Maharashtra Medical Council has granted <span style="color:red">3 credit points</span> for the conference</p>
            </div>
          </div>
         
         <!---signature---->
         
         <div class="certificate-footer text-muted">
                        <div class="sign" style="display: flex;    justify-content: space-around;
              margin-top: 6%;    width: 100%;
              margin: 7% auto;background-color:#eee
              gap: 0px;">
                            <div class="drname" style="text-align:center; display:inline-block;   width: 16%;">
                                <img src="<?=base_url('public/Certificate/final/sig1.png')?>" alt="" style="      margin-bottom: 8px;  width: 88px;
    height: 37px;">
                                <p style="margin: 6px 0;    color: #052a73;    font-family: sans-serif;font-size:13px;
    font-weight: bold;">Dr. Santosh  Bhide </p>
                               <small style="   font-weight: 500;
    font-family: sans-serif;
    color: #353030;"> Hon. President </small>
                            </div>
                           
                            <div class="drname" style="text-align:center; display:inline-block;margin-bottom:-53px;   width: 16%;">
                                <img src="<?=base_url('public/Certificate/final/sig3.png')?>" alt="" style="       margin-bottom: 8px; width: 88px;
    height: 37px;">
                            
       <p style="margin: 6px 0;    color: #052a73;    font-family: sans-serif;font-size:13px;
    font-weight: bold;">Dr.Vardhaman Kankaria  </p>
                               <small style="   font-weight: 500;
    font-family: sans-serif;
    color: #353030;">Hon. Chairman Scientific Committee </small>
                            </div>
                            <div class="drname" style="text-align:center;  display:inline-block;  width: 16%;">
                                <img src="<?=base_url('public/Certificate/final/sig2.png')?>" alt="" style="       margin-bottom: 8px; width: 88px;
    height: 37px;">
                                <p style="margin: 6px 0;    color: #052a73;    font-family: sans-serif;font-size:13px;
    font-weight: bold;">Dr. Anagha Heroor </p>
                               <small style="      font-weight: 500;
    font-family: sans-serif;
    color: #353030;"> Hon. Secretary</small>
                            </div>
                            <div class="drname" style="text-align:center;  display:inline-block;  width: 16%;margin-bottom:-20px;">
                                <img src="<?=base_url('public/Certificate/final/sig5.png')?>" alt="" style="       margin-bottom: 8px; width: 88px;
    height: 37px;">
                                <p style="margin: 6px 0;    color: #052a73;    font-family: sans-serif;font-size:13px;
    font-weight: bold;">Dr. Vivek Motewar </p>
                               <small style="     font-weight: 500;
    font-family: sans-serif;
    color: #353030;"> Hon. Treasurer </small>
                            </div>
                            <div class="drname" style="text-align:center;  display:inline-block;   width: 16%;;margin-bottom:-20px;">
                                <img src="<?=base_url('public/Certificate/final/sig6.png')?>" alt="" style=" margin-top:35px;  display:inline-block; margin-bottom: 8px;  width: 88px;
    height: 37px;">
                                <p style="margin: 6px 0;    color: #052a73;    font-family: sans-serif;font-size:13px;
    font-weight: bold;">Dr. Aditi Watve </p>
                               <small style="     font-weight: 500;
    font-family: sans-serif;
    color: #353030;">Secretary, scientific committee </small>
                            </div>
                             <div class="drname" style="text-align:center;  display:inline-block;   width: 16%;">
                                <img src="<?=base_url('public/Certificate/final/sig_n6.png')?>" alt="" style=" margin-top:35px;  display:inline-block; margin-bottom: 8px;  width: 88px;
    height: 37px;">
                                <p style="margin: 6px 0;    color: #052a73;    font-family: sans-serif;font-size:13px;
    font-weight: bold;">Dr. Nikhil Nasta </p>
                               <small style="     font-weight: 500;
    font-family: sans-serif;font-size:12px;
    color: #353030;">MMC Observer</small>
                            </div>
                            
                             
                            
                            
                        </div>
                    </div>
         <!--signature-->
         
        </div>
      </div>
    </div>
  </body>
</html>