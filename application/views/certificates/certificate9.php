<html>
  <head>
    </style>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans|Pinyon+Script|Rochester">
  </head>
  <body>
    <div class="certificate-container" style="  background-image: url();
      background-size: 100% 100%; padding: 50px;
      width:872px;background-color: #eee;-webkit-print-color-adjust: exact;  print-color-adjust: exact; 
      margin: auto; position: relative;">
      <img src="<?=base_url('public/Certificate/bg.jpg')?>" alt="" style="    position: absolute;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;">
      <div class="certificate" style="padding: 25px;
        height: 635px;
        position: relative;">
        <div class="water-mark-overlay"></div>
        <!-- <a href="#" style="position: absolute;
          right: 28px;
          text-decoration: none;
          background: #2a2738;
          padding: 8px 18px;
          border-radius: 100px;
          color: #fff;">Download Now</a> -->
        <div class="certificate-header" style="    display: flex;
          margin-top: 5%;
          gap: 18px;
          align-items: center;">
          <img src="<?=base_url($profile_pic)?>" class="logo" alt=""> <span style="
            font-size: 40px;
            font-weight: bold;    background: -webkit-linear-gradient(#920836, #022c09);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            "><?=$name?></span>
        </div>
        <div class="certificate-body">
          <h1 style="    font-weight: bold;
            font-size: 86px;
            text-align: center;
            margin: 1% 0;
            /* color: #0C5280; */
            font-family: 'Pinyon Script', cursive;
            /* font-size: 72px; */
            background: -webkit-linear-gradient(#a81835, #022c09);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;">Certificate</h1>
          <div class="" style="display: flex; gap: 40px;justify-content: center;">
            <div class="name" style="    width: 38%;font-family: system-ui;">
              <p class="certificate-title"><strong>1No. MMC/Accre. <?=$mmc_no?> CPD Code -<?=$cpd_code?> Appl no. <?=$application_no?></strong></p>
            </div>
            <div class="name" style="font-family: system-ui;">
              <p class="certificate-title"><strong style="font-weight: 500;"><?=$orgnize_date?>
                </strong>
                <br><span style="font-size: 21px;
                  font-weight: 700;"><?=$title?></span>
              </p>
            </div>
          </div>
          <div class="certificate-content">
            <h3 style="text-align: center;
              font-weight: bold;
              font-size: 31px;
              margin: 0;
              border-bottom: 2px dotted #000;
              width: 56%;
              margin: auto;"><?=$user_name?></h3>
            <div class="text-center">
              <p class="topic-description text-muted" style="     color: #492828;   font-size: 21px;"> 
              <?=$event_contant?>
              </p>
            </div>
          </div>
          <div class="certificate-footer text-muted">
            <div class="sign" style="display: flex;
              margin-top: 6%;
              gap: 17px;">
                 <?php 
                  $signatures =  json_decode($signature);
                  //print_r($signatures);
                  foreach($signatures as $signature_data){
                    foreach($signature_data as $key=>$sign){
                    //print_r($signature);
                  ?>
              <div class="drname">
                <img src="<?=base_url($sign)?>" alt="" style="    width: 88px;
                  height: 37px;">
                <p style="margin: 0;    color: #3c232c;
                  font-weight: bold;"><?=$key?> </p>
                <small style="    font-weight: bold;">Accredited </small>
              </div>
              <?php }}?>
             
              <!-- <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6">
                        <p>
                            Accredited by
                        </p>
                    </div>
                    <div class="col-md-6">
                        <p>
                            Endorsed by
                        </p>
                    </div>
                </div>
                </div> -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>