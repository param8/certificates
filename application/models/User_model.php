<?php 

class User_model extends CI_Model

{

	public function __construct()

	{
		parent::__construct();
	}



	public function get_all_users($condition)
	{
		$this->db->select('users.*,events.*');
		$this->db->from('users');
		$this->db->join('events','events.eventID = users.id','left');
		$this->db->where($condition);
	    $this->db->order_by('users.id','desc');
		return $this->db->get()->result();
	}



	public function get_user($condition){
		$this->db->select('users.*,events.*');
		$this->db->from('users');
		$this->db->join('events','events.eventID = users.id','left');
		$this->db->where($condition);
		return $query =  $this->db->get()->row();
		//echo $this->db->last_query();
	}

	public function get_event_certificate($condition){
		//$this->db->where($condition);
		$this->db->group_by('email');
		$this->db->order_by('id','desc');
		return $this->db->get('event_cerificate')->result();
	}



	public function update_user($data,$condition)
	{
		$this->db->where($condition);
		return $this->db->update('users',$data);
	}

	public function update_user_status($data,$id)
	{
		$this->db->where($id);
		return $this->db->update('users',$data);
     //echo $this->db->last_query();die;
	}

	public function get_user_details($data){
		$this->db->select('users.*,states.name as stateName,cities.city as cityName');
		$this->db->join('states','states.id=users.state','left');
		$this->db->join('cities','cities.id=users.city','left');
		$this->db->where($data);
		$result =  $this->db->get('users');
			return $result->row();	
	}

	public function get_certificates($condition){
		$this->db->select('event_cerificate.*,users.name as userName');
		$this->db->join('users','users.id=event_cerificate.eventID','left');
		$this->db->where($condition);
		$this->db->order_by('event_cerificate.id','desc');	
		return $this->db->get('event_cerificate')->result();
	}

	public function get_cerificate($condition){
		$this->db->where($condition);	
		return $this->db->get('event_cerificate')->row();
	}

	public function store_certificate($data){
		 $this->db->insert('event_cerificate',$data);
		 return $this->db->insert_id();

	}

	public function update_certificate($data,$id){
      $this->db->where('id',$id);
	 return $this->db->update('event_cerificate',$data);
	}
	
	public function stor_enquiry($data){
		return $this->db->insert('contact_us', $data);
	   }

	   public function store_event_detail($data){
		return $this->db->insert('events', $data);
	   }

	   public function update_event_detail($data,$condition){
		$this->db->where($condition);
		return $this->db->update('events', $data);
	   }

	  
}