<?php 
class Subject_model extends CI_Model 
{

  public function __construct()
  {
      parent::__construct();

  }

  public function get_subjects($condition){  
    $this->db->select('subjects.*');
    $this->db->where($condition);
    return $this->db->get('subjects')->result();
}

public function get_subject($condition){  
  $this->db->select('subjects.*');
  $this->db->where($condition);
  return $this->db->get('subjects')->row();
}

public function store_subject($data){  
  return $this->db->insert('subjects',$data);
}

public function update_subject($data,$condition){  
  $this->db->where($condition);
  return $this->db->update('subjects',$data);
}




}