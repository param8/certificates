<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(dirname(__FILE__) . '/dompdf/autoload.inc.php');

use Dompdf\Dompdf;

class Pdf extends Dompdf
{
    function createPDF($html, $filename='', $download=TRUE, $paper='A4', $orientation='landscape'){
        $dompdf = new DOMPDF();
        $dompdf->set_option('isRemoteEnabled',True);
        $dompdf->load_html($html);
        $dompdf->set_paper($paper, $orientation);
        $dompdf->render();
        if($download){
            $output = $dompdf->output();
            file_put_contents('uploads/test/'.$filename, $output);
        }
        else{
            $dompdf->stream($filename.'.pdf', array('Attachment' => 0));
        }
    }
}
?>