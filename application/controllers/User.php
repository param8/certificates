<?php 
class User extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_admin_logged_in();
		$this->load->model('user_model');
		$this->load->model('Auth_model');
		$this->load->model('setting_model');
        $this->load->library('csvimport');
        $this->load->helper('download');
	}

	public function index()
	{	
    $data['page_title'] = 'Event';
    $studentCondition = $this->session->userdata('user_type')=='Event' ? array('users.eventID'=>$this->session->userdata('id') ) : array('users.status' => '1','user_type' => 'Event');
    $data['users'] = $this->user_model->get_all_users($studentCondition);
    $data['events'] = $this->user_model->get_all_users(array('user_type' => 'Event'));
		$this->admin_template('events',$data);
	}

  public function event_cerificates()
	{	
    $data['page_title'] = 'Event Cerificate';
    
    $data['events'] = $this->user_model->get_event_certificate(array('status'=>1));
		$this->admin_template('events-certicate',$data);
	}

  // public function cerificates()
	// {	
  //   $data['page_title'] = 'Certificates';
  //   $studentCondition = $this->session->userdata('user_type')=='Event' ? array('users.eventID'=>$this->session->userdata('id') ) : array('users.status' => '1','user_type' => 'Event');
  //   $data['certificates'] = $this->user_model->get_certificates($studentCondition);
  //   $data['events'] = $this->user_model->get_all_users(array('user_type' => 'Event'));
	// 	$this->admin_template('events-certicate',$data);
	// }

  public function user_cerificates()
	{	
    $id =  $this->uri->segment(3);
    $data['user'] = $this->user_model->get_user(array('users.id' => $id));
		$this->load->view('certificates/certificate1',$data);
	}

  public function cerificates1()
	{	
		$this->load->view('certificates/certificate1');
	}

  public function cerificates2()
	{	
		$this->load->view('certificates/certificate2');
	}

  public function cerificates3()
	{	
		$this->load->view('certificates/certificate3');
	}


  public function cerificates4()
	{	
		$this->load->view('certificates/certificate4');
	}

  public function cerificates5()
	{	
		$this->load->view('certificates/certificate5');
	}

  public function cerificates6()
	{	
		$this->load->view('certificates/certificate6');
	}

	public function students()
	{
 
    $data['page_title'] = 'Student';
    $studentCondition = $this->session->userdata('user_type')=='Vendor' ? array('users.vendorID'=>$this->session->userdata('id') ) : array('users.user_type' => 'Student');
    $data['users'] = $this->user_model->get_students($studentCondition);
    $condition = $this->session->userdata('user_type')=='Vendor' ? array('users.id'=>$this->session->userdata('id') ) : array('users.user_type' => 'Vendor');
    $data['vendors'] = $this->user_model->get_all_users($condition);
    $data['marksheets'] = $this->user_model->download_mark_sheets(array('users.status'=>1));
    $data['admitcards'] = $this->user_model->download_admit_cards(array('users.status'=>1));
		$this->admin_template('students',$data);
	}

	public function vendor()
	{	$data['page_title'] = 'Vendor';
        $data['users'] = $this->user_model->get_all_users(array('users.user_type' => 'Vendor'));
		$this->admin_template('users',$data);
	}

	public function create_user()
	{   
		$data['user_type'] = base64_decode($this->uri->segment(2));
		$data['page_title'] = 'Create '.$data['user_type'];
    $data['vendors'] = $this->user_model->get_all_users(array('users.user_type' => 'Vendor'));
		$this->admin_template('create-user',$data);
	}

  public function get_user_certificate(){
      $email = $this->input->post('email');
    $certificates = $this->user_model->get_certificates(array('event_cerificate.email'=>$email));
    foreach($certificates as $key=>$certificate)
    {
    //     echo '<pre>';
    //   echo base_url('certificate/'.$certificate->id);
      
      if(base_url('certificate/'.base64_encode($certificate->id))==$certificate->pdf_path){
          $url = $certificate->pdf_path;
      }else{
          $url = base_url('uploads/certificate/'.$certificate->pdf_path);
      }
  ?>
  <tr>
    <td><?=$key+1;?></td>
    <td nowrap>		
    <a href="<?=$url?>"  class="btn btn-primary btn-sm" data-toggle="tooltip" target="_blank" title="Download Ceritificate">Certificate  <i class="fa fa-download"></i></a>
    </td>
    <td><?= $certificate->userName?></td>
    <td><?= $certificate->name?></td>
    <td><?= $certificate->email?></td>
    <td><?= $certificate->contact?></td>
    <td><?= date('d-m-Y',strtotime($certificate->created_at));?></td>
  </tr>
  <?php
    } 
 }

	public function store(){
		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$contact = $this->input->post('contact');
		$address= $this->input->post('address');
		$user_type =  'Event';
    $password = $this->input->post('password');
    $title= $this->input->post('title');
    $event_contant= $this->input->post('event_contant');
    $orgnize_date = $this->input->post('orgnize_date');
    $signature= $this->input->post('signature');
    $signature_name= $this->input->post('signature_name');
    $designation= $this->input->post('designation');
    $cerificate = 1;
 

		if(empty($name)){
			echo json_encode(['status'=>403, 'message'=>'Please enter  name']); 	
			exit();
		}

    if(empty($email)){
			echo json_encode(['status'=>403, 'message'=>'Please enter  email address']); 	
			exit();
		}

    $checkEmail = $this->user_model->get_user(array('email'=>$email));
		if($checkEmail){
			echo json_encode(['status'=>403,'message'=>'This email is already in use']);
			exit();
		}


 
    if(empty($contact)){
			echo json_encode(['status'=>403, 'message'=>'Please enter  contact no']); 	
			exit();
		}
	

		$checkMobile = $this->user_model->get_user(array('contact'=>$contact));
		if($checkMobile){
			echo json_encode(['status'=>403,'message'=>'This mobile is already in use']);
			exit();
		}
   
		if(empty($title)){
			echo json_encode(['status'=>403, 'message'=>'Please enter  title']); 	
			exit();
		}

    if(empty($event_contant)){
			echo json_encode(['status'=>403, 'message'=>'Please enter  contant']); 	
			exit();
		}

    if(empty($password)){
			echo json_encode(['status'=>403, 'message'=>'Please enter  password']); 	
			exit();
		}

    if(empty($orgnize_date)){
			echo json_encode(['status'=>403, 'message'=>'Please enter  organize date']); 	
			exit();
		}

    $this->load->library('upload');
    if($_FILES['profile_pic']['name']!= '')
		{
    $config = array(
      'upload_path' 	=> 'uploads/users',
      'file_name' 	=> str_replace(' ','',$name).uniqid(),
      'allowed_types' => 'jpg|jpeg|png|gif|webp',
      'max_size' 		=> '10000000',
    );
        $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('profile_pic'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]);
          exit();
      }
      else
      {
        $type = explode('.',$_FILES['profile_pic']['name']);
        $type = $type[count($type) - 1];
        $image = 'uploads/users/'.$config['file_name'].'.'.$type;
      }
     }
    else{
        $image = 'public/dummy_user.jpg';
    }

    
    if(empty($cerificate)){
			echo json_encode(['status'=>403, 'message'=>'Please choose at certificate sample']); 	
			exit();
		}

// print_r($_FILES['signature']['name']);
// print_r($signature_name);
   $signature_array = array();
    if(!empty($_FILES['signature']['name'])){
      $files =array();
      $uid = uniqid();
       $total_slip = count(array_filter($_FILES['signature']['name']));
      for($i = 0; $i < $total_slip;$i++){
        $filename = $_FILES['signature']['name'][$i];
        $f_maxsize = 41943040;
        $f_ext_allowed = array("jpg", "jpeg", "png", "gif","bmp");
        $f_name_2 = str_replace(" ","_", htmlspecialchars($filename));
        $f_size  =  $filename;
        $f_tmp   =  $_FILES["signature"]['tmp_name'][$i];
        $f_error =  $_FILES["signature"]['error'][$i];
        $f_ext = pathinfo($f_name_2, PATHINFO_EXTENSION); 
        $files[] = $uid.$filename;
        $signature = "uploads/signature/".$uid.$filename;
         move_uploaded_file($f_tmp, "uploads/signature/".$uid.$filename); 
         $signature_array[] = array('0'=>$signature,'1'=>$signature_name[$i],'2'=>$designation[$i]);   
               
      }
  }
  $signature_data = json_encode($signature_array);

	  $ran_id = rand(time(), 100000000);
		$data = array(
			'unique_id'   => $ran_id,
			'name'        => $name,
			'email'       => $email,
			'password'    => md5($password),
			'contact'     =>$contact,
			'address'     => $address,
			'user_type'   => $user_type,
      'profile_pic' => $image,
			'login_status' =>'Offline now',
			'otp' =>0,
		);
		$register = $this->Auth_model->register($data);
   

		if($register){
        $dataEvent = array(
          'eventID'       => $register,
          'title'         => $title,
          'event_contant' => $event_contant,
          'orgnize_date'  => $orgnize_date,
          'signature'     => $signature_data,
          'cerificate'    => $cerificate
        );
        $this->user_model->store_event_detail($dataEvent);
      
		  echo json_encode(['status'=>200, 'message'=>$user_type.' added successfully!']);
		}else{
			echo json_encode(['status'=>302, 'message'=>mysqli_error()]);   
		}
	}


  public function edit_user()
	{   
    $id = base64_decode($this->uri->segment(2));
    $data['user']=$this->user_model->get_user(array('users.id'=>$id));
    $data['user_id']=$id;
		$data['user_type'] = $data['user']->user_type;
		$data['page_title'] = 'Edit '.$data['user_type'];
		$this->admin_template('edit-user',$data);
	}

	public function update(){
    $u_id = $this->input->post('user_id');
		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$contact = $this->input->post('contact');
		$address= $this->input->post('address');
		$user_type =  'Event';
    $title= $this->input->post('title');
    $event_contant= $this->input->post('event_contant');
    $orgnize_date = $this->input->post('orgnize_date');
    $signature= $this->input->post('signature');
    $signature_name= $this->input->post('signature_name');
    $cerificate = $this->input->post('cerificate');
 
    $user = $this->user_model->get_user(array('users.id ' =>$u_id));
		if(empty($name)){
			echo json_encode(['status'=>403, 'message'=>'Please enter  name']); 	
			exit();
		}

    if(empty($email)){
			echo json_encode(['status'=>403, 'message'=>'Please enter  email address']); 	
			exit();
		}

    $checkEmail = $this->user_model->get_user(array('users.email'=>$email,'users.id <>' =>$u_id));
		if($checkEmail){
			echo json_encode(['status'=>403,'message'=>'This email is already in use']);
			exit();
		}


 
    if(empty($contact)){
			echo json_encode(['status'=>403, 'message'=>'Please enter  contact no']); 	
			exit();
		}
	

		$checkMobile = $this->user_model->get_user(array('users.contact'=>$contact,'users.id <>'=>$u_id));
		if($checkMobile){
			echo json_encode(['status'=>403,'message'=>'This mobile is already in use']);
			exit();
		}
   
		if(empty($title)){
			echo json_encode(['status'=>403, 'message'=>'Please enter  title']); 	
			exit();
		}

    if(empty($event_contant)){
			echo json_encode(['status'=>403, 'message'=>'Please enter  contant']); 	
			exit();
		}

    // if(empty($password)){
		// 	echo json_encode(['status'=>403, 'message'=>'Please enter  password']); 	
		// 	exit();
		// }

    if(empty($orgnize_date)){
			echo json_encode(['status'=>403, 'message'=>'Please enter  organize date']); 	
			exit();
		}

    $this->load->library('upload');
    if($_FILES['profile_pic']['name']!= '')
		{
    $config = array(
      'upload_path' 	=> 'uploads/users',
      'file_name' 	=> str_replace(' ','',$name).uniqid(),
      'allowed_types' => 'jpg|jpeg|png|gif|webp',
      'max_size' 		=> '10000000',
    );
        $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('profile_pic'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]);
          exit();
      }
      else
      {
        $type = explode('.',$_FILES['profile_pic']['name']);
        $type = $type[count($type) - 1];
        $image = 'uploads/users/'.$config['file_name'].'.'.$type;
      }
     }elseif(!empty($user->profile_pic)){
        $image = $user->profile_pic;
     }
    else{
        $image = 'public/dummy_user.jpg';
    }

    
    if(empty($cerificate)){
			echo json_encode(['status'=>403, 'message'=>'Please choose at certificate sample']); 	
			exit();
		}

// print_r($_FILES['signature']['name']);
// print_r($signature_name);
  //  $signature_array = array();
  //   if(!empty($_FILES['signature']['name'])){
  //     $files =array();
  //     $uid = uniqid();
  //      $total_slip = count(array_filter($_FILES['signature']['name']));
  //     for($i = 0; $i < $total_slip;$i++){
  //       $filename = $_FILES['signature']['name'][$i];
  //       $f_maxsize = 41943040;
  //       $f_ext_allowed = array("jpg", "jpeg", "png", "gif","bmp");
  //       $f_name_2 = str_replace(" ","_", htmlspecialchars($filename));
  //       $f_size  =  $filename;
  //       $f_tmp   =  $_FILES["signature"]['tmp_name'][$i];
  //       $f_error =  $_FILES["signature"]['error'][$i];
  //       $f_ext = pathinfo($f_name_2, PATHINFO_EXTENSION); 
  //       $files[] = $uid.$filename;
  //       $signature = "uploads/signature/".$uid.$filename;
  //        move_uploaded_file($f_tmp, "uploads/signature/".$uid.$filename); 
  //        $signature_array[] = array($signature_name[$i]=>$signature);   
               
  //     }
  // }
  // $signature_data = json_encode($signature_array);

		$data = array(
			'name'        => $name,
			'email'       => $email,
			'contact'     =>$contact,
			'address'     => $address,
            'profile_pic' =>$image

		);
		$register = $this->user_model->update_user($data,array('id'=>$u_id));
   

		if($register){
        $dataEvent = array(
          'title'         => $title,
          'event_contant' => $event_contant,
          'orgnize_date'  => $orgnize_date,
          //'signature'     => $signature_data,
          'cerificate'    => $cerificate
        );
        $this->user_model->update_event_detail($dataEvent,array('eventID'=>$u_id));
      
		  echo json_encode(['status'=>200, 'message'=>$user_type.' Updated successfully!']);
		}else{
			echo json_encode(['status'=>302, 'message'=>mysqli_error()]);   
		}
	}


  public function view_user()
	{   
    $id = base64_decode($this->uri->segment(2));
    $data['user']=$this->user_model->get_user(array('users.id'=>$id));
    //print_r($data['user']);die;
    $data['user_id']=$data['user']->id;
		$data['user_type'] = $data['user']->user_type;
		$data['page_title'] = 'View '.$data['user_type'];
		$this->admin_template('view-user',$data);
	}

	public function update_status()
	{
		$user_id = $this->input->post('userid');
		$user_status = ($this->input->post('user_status')=='Active')?'1':'0';

        $userdata =  array(
          'status' => $user_status
        );
	
        $update_status =  $this->user_model->update_user_status($userdata,array('id'=>$user_id));
     
	}




public function download_sample(){
  $file_name = 'sample/sample_certificate.csv';
  $file = file_get_contents(base_url($file_name));
  force_download($file_name, $file);
}

public function download_mark_sheet_sample(){
  $file_name = 'sample/student_maksheet_sample.csv';
  $file = file_get_contents(base_url($file_name));
  force_download($file_name, $file);
}

public function download_admit_card_sample(){
  $file_name = 'sample/student_admitcard_sample.csv';
  $file = file_get_contents(base_url($file_name));
  force_download($file_name, $file);
}

public function bulk_import_certificate(){
  $this->load->library('pdf');
  if (isset($_FILES["certificate"])) {
    $config['upload_path']   = "uploads/csv/";
    $config['allowed_types'] = 'text/plain|text/csv|csv';
    $config['max_size']      = '2048';
    $config['file_name']     = $_FILES["certificate"]['name'];
    $config['overwrite']     = TRUE;
    $this->load->library('upload', $config);
    $this->upload->initialize($config);
    if (!$this->upload->do_upload("certificate")) {
      echo json_encode(['status'=>403, 'message'=>'Please Upload Valid CSV files']);
    } else {
      $file_data = $this->upload->data();
      $file_path = 'uploads/csv/' . $file_data['file_name'];
      if ($this->csvimport->get_array($file_path)) {
        $csv_array = $this->csvimport->get_array($file_path);
        foreach ($csv_array as $key => $row) {
        
          $data['name'] = $row['name'];
           $html = $this->load->view('certificate', $data, true);
           $file_name = str_replace(' ', '', $row['name']).'.pdf';
           $this->pdf->createPDF($html, $file_name, TRUE);
          
           $subject  =   "MOSCON 2023 MIDTERM DELEGATES ATTENDANCE CERTIFICATE" ;
           $html = "Dear ".$row['name']."<br>
                   Thank you for participating in 'MOS Midterm - Young Turks'. Herewith I am sharing your certificate with MMC credit points. MMC credit points will alloted to your name in MMC office after their internal processing in approximately 3 weeks time. <br><br>
                   <a href='".$file_name."'><b>Download Certificate</b></a> <br><br>
                   Thanks and Regards <br>  
                   Dr Vardhaman Kankariya<br> 
                   Chairman Scientific Committee <br>
                   MOS <br> On behalf of Team MOS";
           $email = $row['email'];
          // $sendmail = sendEmail($email,$subject,$html);
         
        }
        
      } else {
        echo json_encode(['status'=>403, 'message'=>'Ceriticate Uploaded Failure']);
      }
    }
  } else {
    echo json_encode(['status'=>403, 'message'=>'something went wrong']);
  }
}
  
// public function bulk_import_certificate(){
//   $eventID = $this->input->post('eventID');
//   $user = $this->user_model->get_user(array('users.id' => $eventID));
//   //print_r($user);die;
//   if(empty($eventID)){
//     echo json_encode(['status'=>403, 'message'=>'Please select event Organizer.']); 	
//     exit();
//   }
//   if (isset($_FILES["certificate"])) {
//     $config['upload_path']   = "uploads/csv/";
//     $config['allowed_types'] = 'text/plain|text/csv|csv';
//     $config['max_size']      = '2048';
//     $config['file_name']     = $_FILES["certificate"]['name'];
//     $config['overwrite']     = TRUE;
//     $this->load->library('upload', $config);
//     $this->upload->initialize($config);
//     if (!$this->upload->do_upload("certificate")) {
//       echo json_encode(['status'=>403, 'message'=>'Please Upload Valid CSV files']);
//     } else {
//       $file_data = $this->upload->data();
//       $file_path = 'uploads/csv/' . $file_data['file_name'];
//       if ($this->csvimport->get_array($file_path)) {
//         $csv_array = $this->csvimport->get_array($file_path);
//         foreach ($csv_array as $key => $row) {
        
//           // $checkEmail = $this->user_model->get_user(array('email'=>$row['email']));
//           // if($checkEmail){
//           //   continue;
//           // }

//           //$get_certificate = $this->user_model->get_cerificate(array('status'=>1));
//           //$certificateID  = $get_certificate ? $get_certificate->id+1 : 1;
//           $appication = date('Ymd'); 

//             $this->load->library('pdf');
//             $data['name'] = $user->name;
//             $data['profile_pic'] = $user->profile_pic;
//             $data['title'] = $user->title;
//             $data['event_contant'] = $user->event_contant;
//             $data['orgnize_date'] = $user->orgnize_date;
//             $data['signature'] = $user->signature;
//             $data['user_name'] = $row['name'];
//             //$data['mmc_no'] = $row['mmc_no'];
//             //$data['cpd_code'] = $row['cpd_code'].$certificateID;
//             //$data['application_no'] = $appication;
//             $html = $this->load->view('certificates/certificate'.$user->cerificate, $data, true);
//             $file_name = str_replace(' ', '', $row['name'].$data['name'].$appication).'.pdf';
//             $this->pdf->createPDF($html, $file_name, TRUE);

//             $insert_data = array(
//             'eventID' => $eventID,
//             'name' => $row['name'],
//             'email' =>  $row['email'],
//             //'contact' => $row['contact'],
//             //'mmc_no' => $row['mmc_no'],
//             //'cpd_code' => $row['cpd_code'].$certificateID,
//             //'application_no' => $appication,
//             'pdf_path' =>$file_name,
//           );
//           $register = $this->user_model->store_certificate($insert_data);
//         //   echo "<pre>";
//         //   print_r($insert_data);
//           //$site_name  =   $this->input->post('site_name');
//           $attachment = base_url('uploads/certificate/'.$file_name);
//           $url = base_url('uploads/certificate/'.$file_name);
//           $subject  =   "MOSCON 2023 MIDTERM FACULTY ATTENDANCE CERTIFICATE" ;
//           $html = "Dear ".$row['name']."<br>
//                     Thank you for participating in 'MOS Midterm - Young Turks'. Herewith I am sharing your certificate with MMC credit points. MMC credit points will alloted to your name in MMC office after their internal processing in approximately 3 weeks time. <br><br>
//                     <a href='".$url."'><b>Download Certificate</b></a> <br><br>
//                     Thanks and Regards <br>  
//                     Dr Vardhaman Kankariya<br> 
//                     Chairman Scientific Committee <br>
//                     MOS <br> On behalf of Team MOS";
//           $email = $row['email'];
//           $sendmail = sendEmail($email,$subject,$html); 
//         //   if($sendmail==1){
//         //       echo json_encode(['status'=>200, 'message'=>'Mail Send Successfully']);
//         //   }
//         }
//         echo json_encode(['status'=>200, 'message'=>'Ceriticate Uploaded Successfully']);
//       } else {
//         echo json_encode(['status'=>403, 'message'=>'Ceriticate Uploaded Failure']);
//       }
//     }
//   } else {
//     echo json_encode(['status'=>403, 'message'=>'something went wrong']);
//   }
// }





	public function download_document(){
    $file_name = base64_decode($this->uri->segment(3));
    $file = file_get_contents(base_url($file_name));
    force_download($file_name, $file);
  }

  
  

  
  public function profile(){
    $id = $this->session->userdata('id');
    $data['user']=$this->user_model->get_user(array('users.id'=>$id));
    $data['user_id']=$data['user']->id;
		$data['user_type'] = $data['user']->user_type;
		$data['page_title'] = 'Profile';
    $this->admin_template('profile', $data);
  }

    public function update_profile(){
      $u_id = $this->input->post('user_id');
      $name = $this->input->post('name');
      $contact = $this->input->post('contact');
      $address= $this->input->post('address');
      $user_type = $this->input->post('user_type');
      $state = $this->input->post('state');
      $city = $this->input->post('city');
      $user = $this->user_model->get_user(array('users.id'=>$u_id));
  
      if(empty($name)){
        echo json_encode(['status'=>403, 'message'=>'Please enter '.$user_type.' name']); 	
        exit();
      }
  
      if(empty($contact)){
        echo json_encode(['status'=>403, 'message'=>'Please enter '.$user_type.' mobile']); 	
        exit();
      }
  
      $checkMobile = $this->user_model->get_user(array('users.contact'=>$contact,'users.id<>'=>$u_id));
      if($checkMobile){
        echo json_encode(['status'=>403,'message'=>'This mobile is already in use']);
        exit();
      }
     
      if(empty($address)){
        echo json_encode(['status'=>403, 'message'=>'Please enter  address']); 	
        exit();
      }
  
      if(empty($state)){
        echo json_encode(['status'=>403, 'message'=>'Please slecte state']); 	
        exit();
      }
  
      if(empty($city)){
        echo json_encode(['status'=>403, 'message'=>'Please slecte city']); 	
        exit();
      }
  

  
  
      $this->load->library('upload');
      if($_FILES['profile_pic']['name'] != '')
      {
      $config = array(
        'upload_path' 	=> 'uploads/users',
        'file_name' 	=> str_replace(' ','','profilePIC'.$name).uniqid(),
        'allowed_types' => '*',
        'max_size' 		=> '10000000',
      );
          $this->upload->initialize($config);
      if ( ! $this->upload->do_upload('profile_pic'))
        {
            $error = $this->upload->display_errors();
            echo json_encode(['status'=>403, 'message'=>$error]);
            exit();
        }
        else
        {
          $type = explode('.',$_FILES['profile_pic']['name']);
          $type = $type[count($type) - 1];
          $profile_pic = 'uploads/users/'.$config['file_name'].'.'.$type;
        }
      }elseif(!empty($user->profile_pic)){
        $profile_pic = $user->profile_pic;
      }else{
        $profile_pic = '';
      }
  
      $data = array(
        'name'        => $name,
        'contact'     => $contact,
        'address'     => $address,
        'user_type'   => $user_type,
        'state'       =>$state,
        'city'        =>$city,
        'profile_pic' =>$profile_pic,
      );
      //print_r($data);die;
      $update = $this->user_model->update_user($data,array('id'=>$u_id));

      if($update){
  
        echo json_encode(['status'=>200, 'message'=>$user_type.' Updated successfully!']);
      }else{
        echo json_encode(['status'=>302, 'message'=>mysqli_error()]);   
      }
    }


  }