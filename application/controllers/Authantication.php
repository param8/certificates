<?php 
class Authantication extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();

		//$this->not_admin_logged_in();
		$this->load->model('Auth_model');
		$this->load->model('User_model');
		$this->load->model('Subject_model');
    $this->load->model('setting_model');
	}


	public function index()
	{	$data['page_title'] = 'Login';
		$data['siteinfo'] = $this->siteinfo();
		$this->load->view('layout/login_head',$data);
		$this->load->view('login');
		$this->load->view('layout/login_footer');

	}



	public function login(){
		 $email = $this->input->post('username');
		$password = $this->input->post('password');
		if(empty($email)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your email or contact']); 	
			exit();
		}
		if(empty($password)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your password']); 	
			exit();
		}
		$login = $this->Auth_model->login($email,$password);

		if($login==403){
			echo json_encode(['status'=>403, 'message'=>'email or password incorrect!']);
		}elseif($login==302){
			echo json_encode(['status'=>403, 'message'=>'Your are not active please contact the administrator']);   
		}else{
      echo json_encode(['status'=>200, 'message'=>'Login Successfully','user_type'=>$this->session->userdata('user_type')]);
    }
	}



	public function logout()
	{
		$this->session->sess_destroy();
		redirect('home', 'refresh');
	}

	public function forget_password()
	{	$data['page_title'] = 'Reset Password';
    $this->website_template('user/forget_password',$data);

	}




public function adminIndex()
{	$data['page_title'] = 'Login';
	$data['siteinfo'] = $this->siteinfo();
	$this->load->view('admin/layout/login_head',$data);
	$this->load->view('admin/login');
	$this->load->view('admin/layout/login_footer');
}



public function adminLogout()
{
	$this->session->sess_destroy();
	redirect('login', 'refresh');
}
	
	
}