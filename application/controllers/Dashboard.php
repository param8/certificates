<?php 
class Dashboard extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_admin_logged_in();
		$this->load->model('auth_model');
		$this->load->model('course_model');
		$this->load->model('user_model');
	}

	public function index()
	{	 
		//print_r($this->session->userdata());die;
		 $data['page_title'] = 'Dashboard';
		 $condition = $this->session->userdata('user_type')=='Vendor' ? array('users.vendorID'=>$this->session->userdata('id'),'users.user_type' => 'Student') : array('users.user_type'=>'Student');
		 $data['agents'] = $this->user_model->get_all_users(array('users.user_type' => 'Agent'));
		 $data['users'] = $this->user_model->get_all_users( $condition);
		 $data['vendors'] = $this->user_model->get_all_users(array('users.user_type' => 'Vendor'));
	     $this->admin_template('dashboard',$data);
		
	}

	public function certificate() {
		$this->load->view('certificate');
	}

	public function certificate_download(){
		$certificates =  $this->db->get('event_cerificate')->result();
		//$this->load->helper('download');
    $this->load->library('pdf');
    echo '<pre>';
    //print_r($certificates);
		foreach($certificates as $key=>$certificate){

      print_r($certificate);
			  if(base_url('certificate/'.base64_encode($certificate->id))==$certificate->pdf_path){
          $data['user_name'] = $certificate->name;
          $html = $this->load->view('certificates/certificate', $data, true);
          $file_name = str_replace(' ', '', $certificate->name.date('Ymd')).'.pdf';
          $this->pdf->createPDF($html, $file_name, TRUE);
			   }
			
		}
	 }




}